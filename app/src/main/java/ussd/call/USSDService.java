package ussd.call;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import java.util.Collections;
import java.util.List;


public class USSDService extends AccessibilityService {

    static String between(String value, String a, String b) {
        // Return a substring between the two strings.
        int posA = value.indexOf(a);
        if (posA == -1) {
            return "";
        }
        int posB = value.lastIndexOf(b);
        if (posB == -1) {
            return "";
        }
        int adjustedPosA = posA + a.length();
        if (adjustedPosA >= posB) {
            return "";
        }
        return value.substring(adjustedPosA, posB);
    }

    static String after(String value, String a) {
        // Returns a substring containing all characters after a string.
        int posA = value.lastIndexOf(a);
        if (posA == -1) {
            return "";
        }
        int adjustedPosA = posA + a.length();
        if (adjustedPosA >= value.length()) {
            return "";
        }
        return value.substring(adjustedPosA);
    }


    public static String TAG = USSDService.class.getSimpleName();

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        Log.d(TAG, "onAccessibilityEvent");

        AccessibilityNodeInfo source = event.getSource();
        /* if (event.getEventType() == AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED && !event.getClassName().equals("android.app.AlertDialog")) { // android.app.AlertDialog is the standard but not for all phones  */
        if (event.getEventType() == AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED && !String.valueOf(event.getClassName()).contains("AlertDialog")) {
            return;
        }
        if (event.getEventType() == AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED && (source == null || !source.getClassName().equals("android.widget.TextView"))) {
            return;
        }
        if (event.getEventType() == AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED && TextUtils.isEmpty(source.getText())) {
            return;
        }

        List<CharSequence> eventText;

        if (event.getEventType() == AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED) {
            eventText = event.getText();
        } else {
            eventText = Collections.singletonList(source.getText());
        }

        String text = processUSSDText(eventText);

        String dataUssd = String.valueOf((eventText));

//        int iend = dataUssd.indexOf("[Carrier info, No HP ");
//
        String subString = "";
/*

telkomsel *808#

indosat *123*30#




 */

        String sub1 = "", sub2 = "";

//        dataUssd = "Status Kesiapan 4G 087765453421 :";

        if (dataUssd.contains("Layanan ini dikenakan tarif")) // telkomsel
        {
            sub1 = "Anda";
            sub2 = ". Layanan ini dikenakan";
//            if (subString != null) {
            subString = between(dataUssd, sub1, sub2);
            sendMessageToActivity(subString);
//            }
        } else if (dataUssd.contains("No HP") && dataUssd.contains("Pulsa")) { // indosat
            sub1 = " No HP";
            sub2 = " Pulsa";
//            if (subString != null) {
            subString = between(dataUssd, sub1, sub2);
            sendMessageToActivity(subString);
//            }
        } else if (dataUssd.contains("MSISDN")) {
            sub1 = "MSISDN";

            subString = after(dataUssd,sub1);
            sendMessageToActivity(subString);

        }

        else if (dataUssd.contains("Status Kesiapan 4G"))
        {
            sub1 = "Status Kesiapan 4G";
            sub2 = " :";

            subString = between(dataUssd, sub1, sub2);
            sendMessageToActivity(subString);
        }


        Log.d("Data Parsing : ", subString);


        Log.d("dataussd", String.valueOf(eventText));

        if (TextUtils.isEmpty(text)) return;

        // Close dialog
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            performGlobalAction(GLOBAL_ACTION_BACK); // This works on 4.1+ only
        }

        Log.d("dataussd", text);
        // Handle USSD response here

    }

    private void sendMessageToActivity(String msg) {
        Intent intent = new Intent("datakirim");
        // You can also include some extra data.
        intent.putExtra("Status", msg);
//        Bundle b = new Bundle();
//        b.putParcelable("Location", l);
//        intent.putExtra("Location", b);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private String processUSSDText(List<CharSequence> eventText) {
        for (CharSequence s : eventText) {
            String text = String.valueOf(s);
            // Return text if text is the expected ussd response
            if (true) {
                return text;
            }
        }
        return null;
    }

    @Override
    public void onInterrupt() {
    }

    @Override
    protected void onServiceConnected() {
        super.onServiceConnected();
        Log.d(TAG, "onServiceConnected");
        AccessibilityServiceInfo info = new AccessibilityServiceInfo();
        info.flags = AccessibilityServiceInfo.DEFAULT;
        info.packageNames = new String[]{"com.android.phone"};
        info.eventTypes = AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED | AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED;
        info.feedbackType = AccessibilityServiceInfo.FEEDBACK_GENERIC;
        setServiceInfo(info);
    }
}
